from transformers import pipeline
from diffusers import (
    StableDiffusionControlNetPipeline,
    ControlNetModel,
    UniPCMultistepScheduler,
)


def load_pipeline():
    controlnet = ControlNetModel.from_pretrained("lllyasviel/sd-controlnet-depth")
    pipe = StableDiffusionControlNetPipeline.from_pretrained(
        "Lykon/DreamShaper",
        controlnet=controlnet,
        #     torch_dtype=torch.float32,
        #     use_safetensors=True,
    )

    pipe.load_ip_adapter(
        "h94/IP-Adapter",
        subfolder="models",
        weight_name="ip-adapter-plus-face_sd15.bin",
    )
    pipe.set_ip_adapter_scale(0.3)
    pipe.scheduler = UniPCMultistepScheduler.from_config(pipe.scheduler.config)
    return pipe
