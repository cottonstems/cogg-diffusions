# Predicti on interface for Cog ⚙️
# https://cog.run/python
import base64
from PIL import Image

from cog import BasePredictor, File, Input, Path
from cog.types import tempfile
import numpy as np
import torch
import shortuuid as uuid
from io import BytesIO
from transformers import pipeline
# from pipeline import depth_estimator
from model_pipeline import load_pipeline


def gdepth(image):
    depth_estimator = pipeline("depth-estimation")

    image = debyted(image)

    image = depth_estimator(image)["depth"]
    image = np.array(image)
    image = image[:, :, None]
    image = np.concatenate([image, image, image], axis=2)
    detected_map = torch.from_numpy(image).float() / 255.0
    depth_map = detected_map.permute(2, 0, 1)
    return depth_map


def debyted(inp):
    bdata = inp.split(",")[1].encode("ascii")
    im = Image.open(BytesIO(base64.b64decode(bdata)))

    return im


class Predictor(BasePredictor):
    def setup(self) -> None:
        """Load the model into memory to make running multiple predictions efficient"""
        # self.model = torch.load("./weights.pth")
        self.model = load_pipeline()

    def predict(
        self,
        image: str = Input(description="MAIN image"),
        depth: str = Input(description="DEPTH image"),
        adapter: str = Input(description="ADAPTER image"),
        interferance: int = Input(description="Factor to scale image by"),
        adapter_scale: float = Input(description="Factor to scale image by"),
        prompt: str = Input(description="Prosto prompt"),
        prefix: str = Input(description="Prosto pREFIX"),
    ) -> Path:
        """Run a single prediction on the model"""

        output_dir = Path(tempfile.mkdtemp())
        out_path = output_dir.joinpath(f"{prefix}-{uuid.uuid()}.png")

        self.model.set_ip_adapter_scale(adapter_scale)
        img = (
            self.model(
                prompt,
                image=debyted(image),
                ip_adapter_image=debyted(adapter),
                control_image=gdepth(depth),
                num_inference_steps=interferance,
            )
            .images[0]
            .save(out_path)
        )

        return out_path
